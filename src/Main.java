
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//instanciando objetos
			//lacaio
		CartaLacaio lac1 = new CartaLacaio(1, "Frodo Bolseiro",2,1,1);
		CartaLacaio lac2 = new CartaLacaio(2,"Aragorn", 5, 7, 6);
		CartaLacaio lac3 = new CartaLacaio(3, "Legolas", 8, 4, 6);
		
			//magia
		CartaMagia mag1 = new CartaMagia(20,"You shall not pass", 4, true, 7);
		CartaMagia mag2 = new CartaMagia(11, "Telecinese", 3, false, 2);
		CartaMagia mag3 = new CartaMagia(12, "Katon", 5, true, 4);
		CartaMagia mag4 = new CartaMagia(13, "Suiton", 5, true, 4);
		CartaMagia mag5 = new CartaMagia(14, "Fuuton", 5, true, 4);
		
		//lacaio com construtores reduzidos
		//1
		CartaLacaio lac4 = new CartaLacaio(4, "Druida da Noite",6);
		System.out.println("Lacaio:\n"+lac4);
		
		//2
		System.out.println("Lacaio:\n"+lac1);
		System.out.println("Lacaio:\n"+lac3);
		//Alterar ataque do lac1, colocando o ataque do lac3
		lac1.setAtaque(lac3.getAtaque());
		System.out.println("Lacaio:\n"+lac1);
		
		//3
		System.out.println("Magia:\n"+mag1);
		
		//4
		CartaLacaio lac5 = new CartaLacaio(lac2);
		System.out.println("Lacaio:\n"+lac2);
		System.out.println("Lacaio:\n"+lac5);
		
		//6
		System.out.println("Lacaio:\n"+lac5);
		lac5.buffar(13);
		System.out.println("Lacaio:\n"+lac5);
		
		System.out.println("Lacaio:\n"+lac4);
		lac4.buffar(7, 17);
		System.out.println("Lacaio:\n"+lac4);
		
			//tentando buff nulo
		System.out.println("Lacaio:\n"+lac3);
		lac3.buffar(1,0);
		System.out.println("Lacaio:\n"+lac3);
			//tentando buff negativo
		System.out.println("Lacaio:\n"+lac3);
		lac3.buffar(1, -10);
		System.out.println("Lacaio:\n"+lac3);

	}

}
